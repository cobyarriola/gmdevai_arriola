﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    Animator anim;

    public GameObject player;

    public GameObject bullet;

    public GameObject turret;

    Health health;  

    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        
    }
    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    public GameObject GetPlayer()
    {
        return player;
    }

    void Start()
    {
        anim = this.GetComponent<Animator>();
        health = this.GetComponent<Health>();
    }
    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetFloat("health", health.HP);

        if (health.HP <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.AI;

public class Flee : NPCBaseFSM
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    { 
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        var fleeDirection = (NPC.transform.position - opponent.transform.position).normalized;
        NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation,
                                          Quaternion.LookRotation(fleeDirection),
                                          rotSpeed * Time.deltaTime);
        NPC.transform.Translate(0, 0, Time.deltaTime * fleeSpeed);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

}

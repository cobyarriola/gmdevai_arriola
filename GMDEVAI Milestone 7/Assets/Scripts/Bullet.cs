﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	public float bulletDamage = 5;

	void OnCollisionEnter(Collision col)
    {
		if (col.gameObject.tag == "Player" || col.gameObject.tag == "TankAI")
		{
			col.gameObject.GetComponent<Health>().ReceiveDamage(bulletDamage);
		}
		GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
		Destroy(e, 1.5f);
		Destroy(this.gameObject);
	}

    void Start () {
		
	}
	
	void Update () {
		
	}
}

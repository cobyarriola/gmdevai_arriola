﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float HP = 100;
    void Start()
    {
        
    }

    public void ReceiveDamage(float damage)
    {
        if (HP > 0)
        {
            HP -= damage;
        }
    }

    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    Vector3 movement;
    Vector3 mousePos;
    public Rigidbody rb;
    public float moveSpeed;

    // Update is called once per frame
    void Update()
    {
        UserMovement();
        PlayerMouse();
    }

    void UserMovement()
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        rb.MovePosition(rb.position + movement * moveSpeed * Time.deltaTime);
    }

    void PlayerMouse()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);

        if (playerPlane.Raycast(ray, out float hitDistance))
        {
            Vector3 targetPoint = ray.GetPoint(hitDistance);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            rb.transform.rotation = Quaternion.Slerp(rb.transform.rotation, targetRotation, 5f * Time.deltaTime);
        }
    }
}

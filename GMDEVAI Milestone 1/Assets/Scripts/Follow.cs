﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform player;
    public float rotSpeed;

    void LateUpdate()
    {
        Vector3 lookAtPlayer = new Vector3(player.position.x, 
                                           this.transform.position.y, 
                                           player.position.z);

        Vector3 direction = lookAtPlayer - transform.position;  

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);

        if (Vector3.Distance(lookAtPlayer, transform.position) > 2)
        {
            this.transform.position = Vector3.Lerp(transform.position, player.position, 0.01f);
        }
    }
}
